# -*- coding: utf-8 -*-

import kivy
kivy.require('1.11.0') # replace with your current kivy version !

#sudo vim /usr/local/lib/python2.7/dist-packages/kivy/input/providers/hidinput.py
#invert_y = int(bool(drs('invert_y', 0))) # 1 -> 0

from kivy.app import App
from kivy.uix.button import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.graphics import Rectangle, Color
from kivy.clock import Clock
import socket
import json
from ConfigParser import SafeConfigParser
import argparse
import sys
import urllib
import urllib2
import datetime
import os
import operator
import RPi.GPIO as GPIO
import Adafruit_MCP9808.MCP9808 as MCP
import threading
import traceback
import peewee
from peewee import *
import logging

#sys.stdout = open('logi.txt', 'w')
#logging.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')

class FuncThread(threading.Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        threading.Thread.__init__(self)
 
    def run(self):
        self._target(*self._args)

class Insidetemperature(Model):
	id = PrimaryKeyField(primary_key=True)
	date = DateTimeField(default=datetime.datetime.now)
	tempC = DecimalField(max_digits=6, decimal_places=2)
	tempF = DecimalField(max_digits=6, decimal_places=2)
	tempK = DecimalField(max_digits=6, decimal_places=2)
	
	class Meta:
		database = MySQLDatabase('thermometer', host='10.0.1.5', port=3306, user='thermometer',passwd='thermometer')

class Configuration:
	def __init__(self, path):
		self.path = path
		
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(21, GPIO.OUT)
		
		conf = SafeConfigParser()
		conf.read(path + "/uninfo.cfg")
		
		# Common
		self.common = {}
		self.common['updateInterval'] = conf.getint('common', 'updateInterval')
		self.common['screenTimeout'] = conf.getint('common', 'screenTimeout')
		
		# Temperature
		self.temperature = {}
		self.temperature['unit'] = conf.get('temperature', 'unit')
		
		# Inside temperature
		self.temperature['inside'] = {}
		self.temperature['inside']['decimals'] = conf.getint('insideTemp', 'decimals')
		self.temperature['inside']['DB'] = {}
		self.temperature['inside']['DB']['enabled'] = conf.getboolean('insideTemp', 'saveToDB')
		self.temperature['inside']['DB']['ip'] = conf.get('insideTemp', 'DBIP')
		self.temperature['inside']['DB']['port'] = conf.getint('insideTemp', 'DBPort')
		self.temperature['inside']['DB']['user'] = conf.get('insideTemp', 'DBUser')
		self.temperature['inside']['DB']['passwd'] = conf.get('insideTemp', 'DBPasswd')
		self.temperature['inside']['DB']['database'] = conf.get('insideTemp', 'DBName')
		self.temperature['inside']['DB']['updateInterval'] = conf.getint('insideTemp', 'DBUpdateInterval')
		
		# Making sure, that the table exists
		if (Insidetemperature.table_exists() == False): # Or, create_table(True), will not give an exception
			logging.info("Insidetemperature table doesn't exist, creating...")
			Insidetemperature.create_table()
		
		# Outside temperature
		self.temperature['outside'] = {}
		self.temperature['outside']['ip'] = conf.get('outsideTemp', 'ip')
		self.temperature['outside']['port'] = conf.getint('outsideTemp', 'port')
		self.temperature['outside']['bufferSize'] = conf.getint('outsideTemp', 'bufferSize')
		self.temperature['outside']['message'] = conf.get('outsideTemp', 'message')
		self.temperature['outside']['decimals'] = conf.getint('outsideTemp', 'decimals')
		self.temperature['dataType'] = conf.get('outsideTemp', 'dataType')
		
		# Weather
		self.weatherAPI = {}
		self.weatherAPI['address'] = conf.get('WeatherAPI', 'address')
		self.weatherAPI['APIKey'] = conf.get('WeatherAPI', 'APIKey')
		self.weatherAPI['countryCode'] = conf.get('WeatherAPI', 'countryCode')
		self.weatherAPI['city'] = conf.get('WeatherAPI', 'city')
		self.weatherAPI['units'] = conf.get('WeatherAPI', 'units')
		self.weatherAPI['iconAddress'] = conf.get('WeatherAPI', 'iconAddress')
		self.weatherAPI['iconType'] = conf.get('WeatherAPI', 'iconType')
		self.weatherAPI['iconFolder'] = conf.get('WeatherAPI', 'iconFolder')
		self.weatherAPI['decimals'] = conf.getint('WeatherAPI', 'decimals')
		self.weatherAPI['dayLetters'] = conf.getint('WeatherAPI', 'dayLetters')
		self.weatherAPI['startIconHour'] = conf.getint('WeatherAPI', 'startIconHour')
		self.weatherAPI['endIconHour'] = conf.getint('WeatherAPI', 'endIconHour')
		
		## Busses
		self.BusAPI = {}
		self.BusAPI['address'] = conf.get('BusAPI', 'address')
		self.BusAPI['username'] = conf.get('BusAPI', 'username')
		self.BusAPI['password'] = conf.get('BusAPI', 'password')
		self.BusAPI['format'] = conf.get('BusAPI', 'format')
		
		# Bus 1
		self.BusAPI['Bus1'] = {}
		self.BusAPI['Bus1']['name'] = conf.get('Bus1', 'name')
		self.BusAPI['Bus1']['stopCode'] = conf.get('Bus1', 'stopCode')
		self.BusAPI['Bus1']['busNumber'] = conf.get('Bus1', 'busNumber')
		
		# Bus 2
		self.BusAPI['Bus2'] = {}
		self.BusAPI['Bus2']['name'] = conf.get('Bus2', 'name')
		self.BusAPI['Bus2']['stopCode'] = conf.get('Bus2', 'stopCode')
		self.BusAPI['Bus2']['busNumber'] = conf.get('Bus2', 'busNumber')
		
		# Bus 3
		self.BusAPI['Bus3'] = {}
		self.BusAPI['Bus3']['name'] = conf.get('Bus3', 'name')
		self.BusAPI['Bus3']['stopCode'] = conf.get('Bus3', 'stopCode')
		self.BusAPI['Bus3']['busNumber'] = conf.get('Bus3', 'busNumber')
		
		# Bus 4
		self.BusAPI['Bus4'] = {}
		self.BusAPI['Bus4']['name'] = conf.get('Bus4', 'name')
		self.BusAPI['Bus4']['stopCode'] = conf.get('Bus4', 'stopCode')
		self.BusAPI['Bus4']['busNumber'] = conf.get('Bus4', 'busNumber')

class UNInfoLayout(FloatLayout):
	
	def loadConfiguration(self):
		pathname = os.path.dirname(sys.argv[0])
		self.conf = Configuration(os.path.abspath(pathname))
		
		self.turnOffScheduler = None
		
		# Temperature
		self.sensor = MCP.MCP9808()
		self.sensor.begin()
		self.currentInsideTemp = self.sensor.readTempC()
		# Starting TCP server
		#self.TCPServer()
		TCPServerThread = FuncThread(self.TCPServer)
		# When threads are daemons, they will be killed with ctrl-c, not just the main program
		TCPServerThread.daemon = True
		TCPServerThread.start()
		
		# Latest inside temperature saved to the database
		tempArray = Insidetemperature.select().order_by(Insidetemperature.id.desc()).limit(1)
		if (tempArray.count() > 0):
			self.latestInsideTemp = tempArray[0]
		else:
			self.latestInsideTemp = None
		
		# Adding weather hours to array
		self.weatherHours = []
		weather1 = {}
		weather2 = {}
		weather3 = {}
		weather4 = {}
		weather5 = {}
		weather6 = {}
		weather7 = {}
		
		weather1['text'] = self.firstHourText
		weather1['image'] = self.firstHourWeather
		weather1['temperature'] = self.firstHourTemperature
		self.weatherHours.append(weather1)
		
		weather2['text'] = self.secondHourText
		weather2['image'] = self.secondHourWeather
		weather2['temperature'] = self.secondHourTemperature
		self.weatherHours.append(weather2)
		
		weather3['text'] = self.thirdHourText
		weather3['image'] = self.thirdHourWeather
		weather3['temperature'] = self.thirdHourTemperature
		self.weatherHours.append(weather3)
		
		weather4['text'] = self.fourthHourText
		weather4['image'] = self.fourthHourWeather
		weather4['temperature'] = self.fourthHourTemperature
		self.weatherHours.append(weather4)
		
		weather5['text'] = self.fifthHourText
		weather5['image'] = self.fifthHourWeather
		weather5['temperature'] = self.fifthHourMTemperature
		self.weatherHours.append(weather5)
		
		weather6['text'] = self.sixthHourText
		weather6['image'] = self.sixthHourWeather
		weather6['temperature'] = self.sixthHourTemperature
		self.weatherHours.append(weather6)
		
		# Adding weather days to array
		self.weatherDays = []
		weatherDay1 = {}
		weatherDay2 = {}
		weatherDay3 = {}
		weatherDay4 = {}
		
		weatherDay1['text'] = self.firstDayText
		weatherDay1['image'] = self.firstDayWeather
		weatherDay1['max'] = self.firstDayMaxTemp
		weatherDay1['min'] = self.firstDayMinTemp
		self.weatherDays.append(weatherDay1)
		
		weatherDay2['text'] = self.secondDayText
		weatherDay2['image'] = self.secondDayWeather
		weatherDay2['max'] = self.secondDayMaxTemp
		weatherDay2['min'] = self.secondDayMinTemp
		self.weatherDays.append(weatherDay2)
		
		weatherDay3['text'] = self.thirdDayText
		weatherDay3['image'] = self.thirdDayWeather
		weatherDay3['max'] = self.thirdDayMaxTemp
		weatherDay3['min'] = self.thirdDayMinTemp
		self.weatherDays.append(weatherDay3)
		
		weatherDay4['text'] = self.fourthDayText
		weatherDay4['image'] = self.fourthDayWeather
		weatherDay4['max'] = self.fourthDayMaxTemp
		weatherDay4['min'] = self.fourthDayMinTemp
		self.weatherDays.append(weatherDay4)
	
	def buttonPressed(self, id):
		logging.info("Screen pressed")
		if (self.turnOffScheduler != None):
			Clock.unschedule(self.turnOffScheduler)
		self.turnScreenOn()
		self.turnOffScheduler = Clock.schedule_once(self.turnScreenOff, self.conf.common['screenTimeout'])
	
	def turnScreenOn(self):
		GPIO.output(21, True)
	
	def turnScreenOff(self, dt):
		GPIO.output(21, False)
		self.turnOffScheduler = None
	
	def bus1Info(self):
		logging.info("Bus 1 pressed")
		#self.insideTemp.text = "asdf"
	def bus2Info(self):
		logging.info("Bus 2 pressed")
	def bus3Info(self):
		logging.info("Bus 3 pressed")
	def bus4Info(self):
		logging.info("Bus 4 pressed")
	def weatherInfo(self):
		logging.info("Weather pressed")
	
	def setValues(self, dt):
		# Bus names
		self.bus1Label.text = self.conf.BusAPI['Bus1']['name']
		self.bus2Label.text = self.conf.BusAPI['Bus2']['name']
		self.bus3Label.text = self.conf.BusAPI['Bus3']['name']
		self.bus4Label.text = self.conf.BusAPI['Bus4']['name']
	
	def update(self, dt):
		#print("Updating")
		try:
			self.getOutsideTemperature()
		except Exception as e:
			# Program crashed for some reason, saving to log
			logging.error("Crashed in getOutsideTemperature")
			logging.error("End of crashed in getOutsideTemperature")
		try:
			self.getInsideTemperature()
		except Exception as e:
			# Program crashed for some reason, saving to log
			logging.error("Crashed in getInsideTemperature")
			logging.error("End of crashed in getInsideTemperature")
		try:
			self.getBus1Departures()
		except Exception as e:
			# Program crashed for some reason, saving to log
			logging.error("Crashed in getBus1Departures")
			logging.error("End of crashed in getBus1Departures")
		try:
			self.getBus2Departures()
		except Exception as e:
			# Program crashed for some reason, saving to log
			logging.error("Crashed in getBus2Departures")
			logging.error("End of crashed in getBus2Departures")
		try:
			self.getBus3Departures()
		except Exception as e:
			# Program crashed for some reason, saving to log
			logging.error("Crashed in getBus3Departures")
			logging.error("End of crashed in getBus3Departures")
		try:
			self.getBus4Departures()
		except Exception as e:
			# Program crashed for some reason, saving to log
			logging.error("Crashed in getBus4Departures")
			logging.error("End of crashed in getBus4Departures")
		try:
			self.getWeather()
		except Exception as e:
			# Program crashed for some reason, saving to log
			logging.error("Crashed in getWeather")
			logging.error("End of crashed in getWeather")
	
	def getWeather(self):
		weatherUrl = self.conf.weatherAPI['address'] + "?q=" + self.conf.weatherAPI['city'] + "," + self.conf.weatherAPI['countryCode'] + "&appid=" + self.conf.weatherAPI['APIKey'] + "&units=" + self.conf.weatherAPI['units']
		wj = json.load(urllib2.urlopen(weatherUrl))
		
		if ('cod' in wj and wj['cod'] == "200" and 'list' in wj and len(wj['list']) > 0):
			i = 0
			# Min and max temp for 3 day forecast
			today = datetime.date.today()
			day1 = today + datetime.timedelta(days = 1)
			day2 = today + datetime.timedelta(days = 2)
			day3 = today + datetime.timedelta(days = 3)
			day4 = today + datetime.timedelta(days = 4)
			day5 = today + datetime.timedelta(days = 5) # Used to check if we went too far
			startIconTime = datetime.time(self.conf.weatherAPI['startIconHour'], 0, 0, 0)
			endIconTime = datetime.time(self.conf.weatherAPI['endIconHour'], 0, 0, 0)
			day1Min = None
			day1Max = None
			day1Icons = {}
			day2Min = None
			day2Max = None
			day2Icons = {}
			day3Min = None
			day3Max = None
			day3Icons = {}
			day4Min = None
			day4Max = None
			day4Icons = {}
			
			for x in wj['list']:
				date = datetime.datetime.utcfromtimestamp(x['dt'])
				if (date.date() == day5):
					break
				
				#print (date)
				tempMin = x['main']['temp_min']
				tempMax = x['main']['temp_max']
				temperature = x['main']['temp']
				weatherIconId = x['weather'][0]['icon'] if 'weather' in x and len(x['weather']) > 0 and 'icon' in x['weather'][0] else None
				#print("Date \"" + str(date) + "\" has icon \"" + weatherIconId + "\"")
				
				# If this is one of the 4 day forecasts (starting from tomorrow), then setting min max
				if (date.date() == day1):
					if (day1Min == None or tempMin < day1Min):
						day1Min = tempMin
					if (day1Max == None or tempMax > day1Max):
						day1Max = tempMax
					# Setting icon (06:00 - 21:00, adding to amount if already exists
					if (startIconTime <= date.time() and date.time() <= endIconTime):
						#print("Adding day1 icon: \"" + weatherIconId + "\"")
						if (weatherIconId in day1Icons):
							day1Icons[weatherIconId] += day1Icons[weatherIconId]
						else:
							day1Icons[weatherIconId] = 1
					else:
						#print("Not adding day1 icon: \"" + weatherIconId + "\"")
						pass
				if (date.date() == day2):
					if (day2Min == None or tempMin < day2Min):
						day2Min = tempMin
					if (day2Max == None or tempMax > day2Max):
						day2Max = tempMax
					# Setting icon (06:00 - 21:00, adding to amount if already exists
					if (startIconTime <= date.time() and date.time() <= endIconTime):
						if (weatherIconId in day2Icons):
							day2Icons[weatherIconId] += day2Icons[weatherIconId]
						else:
							day2Icons[weatherIconId] = 1
				if (date.date() == day3):
					if (day3Min == None or tempMin < day3Min):
						day3Min = tempMin
					if (day3Max == None or tempMax > day3Max):
						day3Max = tempMax
					# Setting icon (06:00 - 21:00, adding to amount if already exists
					if (startIconTime <= date.time() and date.time() <= endIconTime):
						if (weatherIconId in day3Icons):
							day3Icons[weatherIconId] += day3Icons[weatherIconId]
						else:
							day3Icons[weatherIconId] = 1
				if (date.date() == day4):
					if (day4Min == None or tempMin < day4Min):
						day4Min = tempMin
					if (day4Max == None or tempMax > day4Max):
						day4Max = tempMax
					# Setting icon (06:00 - 21:00, adding to amount if already exists
					if (startIconTime <= date.time() and date.time() <= endIconTime):
						if (weatherIconId in day4Icons):
							day4Icons[weatherIconId] += day4Icons[weatherIconId]
						else:
							day4Icons[weatherIconId] = 1
				
				weatherText = x['weather'][0]['description'] if 'weather' in x and len(x['weather']) > 0 and 'description' in x['weather'][0] else ""
				
				weatherIconPath = self.getWeatherIconPath(weatherIconId)
				
				# Setting weather values, first 4 with 3 hour interval
				if (i < 6):
					self.weatherHours[i]['text'].text = date.strftime('%H:%M')
					self.weatherHours[i]['temperature'].text = str(self.getRoundedTemp(temperature)) + " " + self.conf.temperature['unit']
					
					self.weatherHours[i]['image'].source = self.getWeatherIconPath(weatherIconId)
				i += 1
			
			# Setting last 4 day values
			dayLetters = self.conf.weatherAPI['dayLetters']
			self.weatherDays[0]['text'].text = day1.strftime("%A")[:dayLetters]
			self.weatherDays[0]['max'].text = str(self.getRoundedTemp(day1Max)) + " " + self.conf.temperature['unit']
			self.weatherDays[0]['min'].text = str(self.getRoundedTemp(day1Min)) + " " + self.conf.temperature['unit']
			day1Icon = max(day1Icons.iteritems(), key=operator.itemgetter(1))[0] if len(day1Icons) > 0 else None
			self.weatherDays[0]['image'].source = self.getWeatherIconPath(day1Icon)
			
			self.weatherDays[1]['text'].text = day2.strftime("%A")[:dayLetters]
			self.weatherDays[1]['max'].text = str(self.getRoundedTemp(day2Max)) + " " + self.conf.temperature['unit']
			self.weatherDays[1]['min'].text = str(self.getRoundedTemp(day2Min)) + " " + self.conf.temperature['unit']
			day2Icon = max(day2Icons.iteritems(), key=operator.itemgetter(1))[0] if len(day2Icons) > 0 else None
			self.weatherDays[1]['image'].source = self.getWeatherIconPath(day2Icon)
			
			self.weatherDays[2]['text'].text = day3.strftime("%A")[:dayLetters]
			self.weatherDays[2]['max'].text = str(self.getRoundedTemp(day3Max)) + " " + self.conf.temperature['unit']
			self.weatherDays[2]['min'].text = str(self.getRoundedTemp(day3Min)) + " " + self.conf.temperature['unit']
			day3Icon = max(day3Icons.iteritems(), key=operator.itemgetter(1))[0] if len(day3Icons) > 0 else None
			self.weatherDays[2]['image'].source = self.getWeatherIconPath(day3Icon)
			
			self.weatherDays[3]['text'].text = day4.strftime("%A")[:dayLetters]
			self.weatherDays[3]['max'].text = str(self.getRoundedTemp(day4Max)) + " " + self.conf.temperature['unit']
			self.weatherDays[3]['min'].text = str(self.getRoundedTemp(day4Min)) + " " + self.conf.temperature['unit']
			day4Icon = max(day4Icons.iteritems(), key=operator.itemgetter(1))[0] if len(day4Icons) > 0 else None
			self.weatherDays[3]['image'].source = self.getWeatherIconPath(day4Icon)
	
	def getRoundedTemp(self, temperature):
		decimals = self.conf.weatherAPI['decimals']
		roundedTemp = round(temperature, decimals)
		if (decimals == 0):
			roundedTemp = int(roundedTemp)
		
		return roundedTemp
	
	def getWeatherIconPath(self, weatherIconId):
		weatherIconPath = ""
		if (weatherIconId != None):
			# If icon doesn't exist, downloading it
			weatherIconPath = self.conf.path + "/" + self.conf.weatherAPI['iconFolder'] + "/" + weatherIconId + "." + self.conf.weatherAPI['iconType']
			if (os.path.isfile(weatherIconPath) == False):
				weatherIconUrl = self.conf.weatherAPI['iconAddress'] + weatherIconId + "." + self.conf.weatherAPI['iconType']
				urllib.urlretrieve(weatherIconUrl, weatherIconPath)
			
		return weatherIconPath
	
	def getBus1Departures(self):
		busDepartures = self.getBusDepartures(self.conf.BusAPI['Bus1']['stopCode'], self.conf.BusAPI['Bus1']['busNumber'])
		self.bus1Departure1.text = busDepartures[0] if len(busDepartures) > 0 else ""
		self.bus1Departure2.text = busDepartures[1] if len(busDepartures) > 1 else ""
		self.bus1Departure3.text = busDepartures[2] if len(busDepartures) > 2 else ""
		self.bus1Departure4.text = busDepartures[3] if len(busDepartures) > 3 else ""
	
	def getBus2Departures(self):
		busDepartures = self.getBusDepartures(self.conf.BusAPI['Bus2']['stopCode'], self.conf.BusAPI['Bus2']['busNumber'])
		self.bus2Departure1.text = busDepartures[0] if len(busDepartures) > 0 else ""
		self.bus2Departure2.text = busDepartures[1] if len(busDepartures) > 1 else ""
		self.bus2Departure3.text = busDepartures[2] if len(busDepartures) > 2 else ""
		self.bus2Departure4.text = busDepartures[3] if len(busDepartures) > 3 else ""
	
	def getBus3Departures(self):
		busDepartures = self.getBusDepartures(self.conf.BusAPI['Bus3']['stopCode'], self.conf.BusAPI['Bus3']['busNumber'])
		self.bus3Departure1.text = busDepartures[0] if len(busDepartures) > 0 else ""
		self.bus3Departure2.text = busDepartures[1] if len(busDepartures) > 1 else ""
		self.bus3Departure3.text = busDepartures[2] if len(busDepartures) > 2 else ""
		self.bus3Departure4.text = busDepartures[3] if len(busDepartures) > 3 else ""
	
	def getBus4Departures(self):
		busDepartures = self.getBusDepartures(self.conf.BusAPI['Bus4']['stopCode'], self.conf.BusAPI['Bus4']['busNumber'])
		self.bus4Departure1.text = busDepartures[0] if len(busDepartures) > 0 else ""
		self.bus4Departure2.text = busDepartures[1] if len(busDepartures) > 1 else ""
		self.bus4Departure3.text = busDepartures[2] if len(busDepartures) > 2 else ""
		self.bus4Departure4.text = busDepartures[3] if len(busDepartures) > 3 else ""
	
	# Returns true if "bus" is found in "busArray"
	def isBus(self, bus, busArray):
		return len(filter(lambda x : x == bus, busArray)) > 0
	
	def getBusDepartures(self, stopCode, busNumber):
		
		returnValue = []
		try:
			BusAPI = self.conf.BusAPI
			
			jsonUrl = BusAPI['address'] + "?user=" + BusAPI['username'] + "&pass=" + BusAPI['password'] + "&format=" + BusAPI['format'] + "&request=stop&code=" + stopCode + "&dep_limit=20"
			busJson = json.load(urllib2.urlopen(jsonUrl))
			busArray = busNumber.split("|")
			
			if len(busJson) > 0:
				for x in busJson[0]['departures']:
					if (self.isBus(x['code'], busArray)):
						time = x['time']
						time = time[:2] + ":" + time[2:]
						returnValue.append(time)
						if (len(returnValue) >= 4):
							break
		except Exception as e:
			logging.error("getBusDepartures crashed")
		
		return returnValue
	
	def getInsideTemperature(self):
		try:
			logging.info("Getting inside temp")
			self.currentInsideTemp = self.sensor.readTempC()
			self.insideTemp.text = str(round(self.currentInsideTemp, self.conf.temperature['inside']['decimals'])) + self.conf.temperature['unit']
		except Exception as e:
			logging.error("Crashed while getting inside temp from sensor")
		try:
			# Saving inside temperature to database
			tempC = round(self.currentInsideTemp, self.conf.temperature['inside']['decimals'])
			tempF = round((self.currentInsideTemp * 9/5 + 32), self.conf.temperature['inside']['decimals'])
			tempK = round((self.currentInsideTemp + 273.15), self.conf.temperature['inside']['decimals'])
			self.SaveTempToDatabase(tempC, tempF, tempK)
		except Exception as e:
			logging.error("Crashed while saving inside temperature to the database")
	
	def SaveTempToDatabase(self, tempC, tempF, tempK):
		#self.latestInsideTemp = Insidetemperature.select().order_by(Insidetemperature.id.desc()).limit(1)
		if (self.conf.temperature['inside']['DB']['enabled']):
			# Checking previous time saving temp to database is larger than db interval
			if (self.latestInsideTemp == None or (datetime.datetime.now() - self.latestInsideTemp.date).total_seconds() > self.conf.temperature['inside']['DB']['updateInterval']):
				try:
					logging.info('Saving %sC/%sF/%sK to database', str(tempC), str(tempF), str(tempK))
					temperature = Insidetemperature()
					temperature.tempC = tempC
					temperature.tempF = tempF
					temperature.tempK = tempK
					temperature.save()
					logging.info("Inside values saved to database successfully")
					self.latestInsideTemp = temperature
				except:
					logging.error("Inside temperature could not be saved to the database!")
			else:
				logging.info("Not saving new temps, update interval hasn't passed yet")
	
	def getOutsideTemperature(self):
		TCP_IP = self.conf.temperature['outside']['ip']
		TCP_PORT = self.conf.temperature['outside']['port']
		BUFFER_SIZE = self.conf.temperature['outside']['bufferSize']
		MESSAGE = self.conf.temperature['outside']['message']
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((TCP_IP, TCP_PORT))
		s.send(MESSAGE)
		data = s.recv(BUFFER_SIZE)
		if (data != "Unknown request!"):
			jsonData = json.loads(data)
			newTemp = str(round(jsonData[self.conf.temperature['dataType']], self.conf.temperature['outside']['decimals']))
			#print ('Temperature: ' + newTemp + " " + self.conf.temperature['unit'])
			self.outsideTemp.text = newTemp + " " + self.conf.temperature['unit']
		s.close()
	
	def TCPServer(self):
		TCP_IP = '10.0.2.15' # The port of the server
		TCP_PORT = 5015
		BUFFER_SIZE = 10240  # Max chars for message

		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.bind((TCP_IP, TCP_PORT))
		logging.info('starting listen')
		s.listen(1)
		logging.info('listen started')
		while 1:
			conn, addr = s.accept()
			try:
				logging.info('Connection address:' + str(addr[0]))
				while 1:
					data = conn.recv(BUFFER_SIZE)
					if not data: break
					logging.info("received data: \"" + repr(data.strip()[2:]) + "\"")
					# [2:] removes the first 2 chars, which are hex-values if message is from java,
					# since writeUTF add's 2 hex-values telling the length of the string
					if (data.strip()[2:] == "getTemperature" or data.strip() == "getTemperature"):
						returnJSON = json.JSONEncoder().encode({
							"success": True,
							"message": "",
							"temperatureC": self.currentInsideTemp
						})
						conn.send(str(returnJSON + "\n"))
					else:
                                                message = "Unkown request! Got" + data.strip()
						returnJSON = json.JSONEncoder().encode({
							"success": False,
							"message": message
						})
						logging.info("Sending: " + str(returnJSON))
						conn.send(str(returnJSON) + "\n")
			finally:
				logging.info("Closing TCP server connection")
				conn.close()

class UninfoApp(App):
	def build(self):
		unInfo = UNInfoLayout()
		
		unInfo.loadConfiguration()
		
		# Updater, get buses, temperature, weather
		Clock.schedule_once(unInfo.setValues)
		Clock.schedule_once(unInfo.update)
		Clock.schedule_interval(unInfo.update, unInfo.conf.common['updateInterval'])
		
		return unInfo
	
if __name__ == "__main__":
	try:
		uninfo = UninfoApp()
		
		uninfo.run()
	except KeyboardInterrupt:
		logging.info("Exiting...")

	except Exception as e:
		# Program crashed for some reason, saving to log
		print("Crashed")
	finally:
		print("Cleaning GPIO")
		GPIO.cleanup()
