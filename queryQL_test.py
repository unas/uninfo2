from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport

_transport = RequestsHTTPTransport(
    url='https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql',
    use_json=True,
)

client = Client(
    transport=_transport,
    fetch_schema_from_transport=True,
)

query = gql("""
{
    stop(id: "HSL:1040129") {
        name
        lat
        lon
        wheelchairBoarding
    }
}
""")

stop = client.execute(query)

print(stop["stop"]["name"])